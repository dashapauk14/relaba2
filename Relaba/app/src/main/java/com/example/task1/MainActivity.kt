package com.example.task1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    lateinit var input:EditText
    lateinit var button:Button
    lateinit var output:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        input = findViewById(R.id.input)
        button = findViewById(R.id.button)
        output = findViewById(R.id.output)

        button.setOnClickListener {
            var txt = input.text.toString().trim()
            var count = txt.split("\\W+".toRegex()).count()
            output.text = "Количество слов = $count"

        }
    }
}